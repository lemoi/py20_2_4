from pprint import pprint

def get_frequency_words(words, count=10, length=7):
  words_long = [word for word in words if len(word) >= length]
  words_dict = dict((x, words_long.count(x)) for x in set(words_long))
  words_sorted = sorted(words_dict.items(), key=lambda item: (item[1], item[0]), reverse=True)

  # return words_sorted[:count]
  return [item[0] for item in words_sorted[:count]]


def proc1():
  import json

  with open("files/newsafr.json", encoding="utf_8") as datafile:
    json_data = json.load(datafile)

  words = []
  for item in json_data['rss']['channel']['items']:
    # words.extend(item['title'].split())
    words.extend(item['description'].split())

  return get_frequency_words(words)


def proc2():
  import xml.etree.ElementTree as ET

  parser = ET.XMLParser(encoding="utf-8")
  tree = ET.parse('files/newsafr.xml', parser)

  words = []
  for item in tree.iterfind('channel/item'):
    # words.extend(item.find('title').text.split())
    words.extend(item.find('description').text.split())

  return get_frequency_words(words)

print('Задача №1. Чтение файла в формате json')
pprint(proc1())

print('\nЗадача №2. Чтение файла в формате xml')
pprint(proc2())
